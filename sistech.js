const listGroup = document.querySelector('.list-group');
const addPost = document.querySelector('.post');
const editPost = document.querySelector('.edit-post');
const url = 'https://sistech-api.vercel.app/blog/';
let output = '';

const getResponse = response => response.json();
const processJSON = json => {
  if(!!Object.keys(json).length) {
    output = `
      <li class="list-group-item">${json.title}</li>
      <li class="list-group-item">${json.content}</li>
    `
  }
  listGroup.innerHTML = output;
}
const writeServer = (action, data) => ({
  method: action,
  body: JSON.stringify(data),
  headers: {
    'Content-Type': 'application/json; charset=UTF-8',
    'Authorization': 'Bearer 8c8e888f-1ac2-465d-b840-543e028e3461',
  }
});

// Array untuk menyimpan blog
var blogs = [];

fetch(url, writeServer('GET'))
.then(getResponse)
.then(displayBlogs);

// Function untuk menampilkan blog
// Function to display blogs
function displayBlogs(json) {
    var blogList = document.getElementById("blog-list");
    blogList.innerHTML = "";
  
    if (Array.isArray(json)) {
      json.forEach(blog => {
        var blogDiv = document.createElement("div");
        blogDiv.classList.add("blog-post");
  
        var titleElem = document.createElement("h3");
        titleElem.classList.add("blog-title");
        titleElem.textContent = blog.title;
  
        var contentElem = document.createElement("p");
        contentElem.classList.add("blog-content");
        contentElem.textContent = blog.content;
  
        var editButton = document.createElement("button");
        editButton.textContent = "Edit";
        editButton.addEventListener("click", function() {
            
          editBlog(blog);
        });
  
        blogDiv.appendChild(titleElem);
        blogDiv.appendChild(contentElem);
        blogDiv.appendChild(editButton);

  
        blogList.appendChild(blogDiv);
      });
    }
  }
// Function untuk menambah blog
function addBlog(event) {
  event.preventDefault();

  var titleInput = document.getElementById("title");
  var contentInput = document.getElementById("content");

  var blog = {
    title: titleInput.value,
    content: contentInput.value
  };

  const updatePost = {
    title: titleInput.value,
    body: contentInput.value
  }
  fetch(url, writeServer('POST', blog))
  .then(getResponse)
  .then(() => {
    titleInput.value = '';
    contentInput.value = '';
    displayBlogs();
    location.reload();
  });

  titleInput.value = "";
  contentInput.value = "";
}

// Function untuk mengubah/mengedit blog
function editBlog(blog) {
    var titleInput = document.getElementById("title");
    var contentInput = document.getElementById("content");
  
    titleInput.value = blog.title;
    contentInput.value = blog.content;
  
    const updatePost = {
      id : blog.id,
      title: titleInput.value,
      content: contentInput.value
    };
    fetch(`${url}`, writeServer('PUT', updatePost))
    .then(getResponse)
    
    // displayBlogs();


  }

  function likeBlog(blogId) {

    const updatePost = {
      id : blogId,
    };

    fetch(`${url}/like`, writeServer('PUT'), updatePost)
      .then(getResponse)
      .then(displayBlogs);
  }
  
// Memanggil function addBlog saat form di-submit
var form = document.getElementById("post-form");
form.addEventListener("submit", addBlog);

// Menangkap elemen-elemen navbar
var btnHome = document.getElementById("btn-home");
var btnBlog = document.getElementById("btn-blog");
var btnAbout = document.getElementById("btn-about");

